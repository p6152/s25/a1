	// Answer #2
 	db.fruits.aggregate([ {$match: {"onSale": true}}, {
      $count: "FruitsOnSale" } ])


	// Answer #3
	db.fruits.aggregate([ {$match: {"stock": {$gt: 20}}}, {$count: "enoughStock"} ])

	// Answer #4
	db.fruits.aggregate([ {$match: {"onSale": true}}, {$group: { _id: null, avg_price: { $ave: "$stock" }}},])

	// Answer #5
	db.fruits.aggregate( {$match: {"supplier": null}},{$group:{_id: null,maxPrice: 
		{ $max: "$price"}}})

	// Answer #6
	db.fruits.aggregate( {$match: {"supplier": null}},{$group:{_id: null,minPrice: 
		{ $min: "$price"}}})